Rails.application.routes.draw do
  root 'home#index'

  get '/surveys', to: 'survey#table', via: :all
  get '/charts', to: 'charts#charts', via: :all

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'

  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'


  resources :users
end
