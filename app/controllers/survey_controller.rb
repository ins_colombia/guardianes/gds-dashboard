class SurveyController < ApplicationController
  include DiseaseDiagnosticConcern
  include DiseaseEndpointConcern
  include SymptomEndpointConcern
  include SurveyEndpointConcern
  skip_before_action :verify_authenticity_token
  before_action :authorize

  public

  def table
    @symptoms = get_symptoms
    @surveys = get_surveys
    @diseases = get_diseases
  end

  helper_method :get_map_link

  def get_map_link(lat, lon)
    "http://osm.org/go/" + get_encoded_path(lat, lon) + "?m="
  end

  helper_method :get_days_difference

  def get_days_difference(from, to)
    from = from.to_time if from.respond_to?(:to_time)
    to = to.to_time if to.respond_to?(:to_time)
    unless from.nil? or to.nil?
      from, to = to, from if from > to
      (((to - from).abs) / 86400).round
    end
  end

  private

  def get_encode_array
    ("A".."Z").to_a + ("a".."z").to_a + ("0".."9").to_a + ["_", "~"]
  end

  def get_encoded_path(lat, lon)
    code = interleave_bits(((lon + 180.0) * 2**32 / 360.0).to_i,
                           ((lat + 90.0) * 2**32 / 180.0).to_i)
    str = ""
    (25 / 3.0).ceil.times do |i|
        digit = (code >> (58 - 6 * i)) & 0x3f
        str << get_encode_array[digit]
    end

    (25 % 3).times { str << "-" }

    str
  end

  def interleave_bits(x, y)
      c = 0

      31.downto(0) do |i|
          c = (c << 1) | ((x >> i) & 1)
          c = (c << 1) | ((y >> i) & 1)
      end

      c
  end
end
