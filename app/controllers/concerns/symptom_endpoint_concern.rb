module SymptomEndpointConcern
  include EndpointConcern

  def get_symptoms
    decode_as_json("dashboard/symptoms")
  end
end
