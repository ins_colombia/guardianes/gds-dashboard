module ProfileEndpointConcern
  include EndpointConcern

  def get_profiles
    decode_as_json("dashboard/profiles")
  end
end
