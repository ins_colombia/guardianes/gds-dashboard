module DiseaseDiagnosticConcern
  extend ActiveSupport::Concern

  included do
    helper_method :get_diagnostic
  end

  def get_diagnostic(disease, survey)
    has_disease = "Si"
    if disease.has_key? "symptoms"
      disease["symptoms"].each do |key, symptom|
        unless survey["report"]["symptoms"].has_key? key
          has_disease = "No"
          break
        end
      end
    elsif
      has_disease = "No"
    end
    has_disease
  end
end
