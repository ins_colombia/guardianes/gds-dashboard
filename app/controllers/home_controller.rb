class HomeController < ApplicationController
  include DiseaseDiagnosticConcern
  include DiseaseEndpointConcern
  include SurveyEndpointConcern
  skip_before_action :verify_authenticity_token
  before_action :authorize

  public

  def index
    @surveys = get_surveys
    @diseases = get_diseases
  end

  helper_method :get_survey_info

  def get_survey_info(key, survey, diseases)
    info = "<p><strong>Fecha de registro</strong>: "
    info += DateTime.parse(survey["createdAt"]).strftime("%d/%m/%Y")
    info += "<br /><strong>Estado de salud</strong>: "
    if survey["isGoodReport"] == false
      info += "Mal"
      disease_info = ""
      has_disease = false
      diseases.each do |key, disease|
        if get_diagnostic(disease, survey) == "Si"
          has_disease = true
          disease_info += "<br /> - " + disease["name"]
        end
      end
      if has_disease
        info += "<br /><strong>Síntomas</strong>:"
        info += disease_info
      end
    else
      info += "Bien"
    end
    user = survey["profile"]
    unless user.nil?
      info += "<br /><strong>Sexo</strong>: "
      info += user["gender"]
      info += "<br /><strong>Edad</strong>: "
      info += user["age"].to_s
    end
    info += "<br /><a href=\"/surveys?s=" + key + "\" target=\"_blank\">Ver registro</a>"
    info += "</p>"
    info
  end
end
