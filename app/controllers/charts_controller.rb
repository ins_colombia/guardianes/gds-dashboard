class ChartsController < ApplicationController
  include SurveyEndpointConcern
  include ProfileEndpointConcern
  include DiseaseEndpointConcern
  include DiseaseDiagnosticConcern
  include SummaryEndpointConcern
  skip_before_action :verify_authenticity_token
  before_action :authorize

  public

  def charts
    @surveys = get_surveys
    @quantity_of_surveys = @surveys.count

    @users = get_profiles
    @quantity_of_users = @users.count

    @diseases = get_diseases
    @quantity_of_diseases = @diseases.count

    @summary = get_summary

    @xls = get_xls_header
    @xls += get_xls_single_number_style("sd", "Short Date")

    # Chart: Reports x Day

    @last_month_label = ""
    @good_reports_from_last_month = ""
    @bad_reports_from_last_month = ""

    xls_reports_per_day_label = get_xls_row_start
    xls_reports_per_day_good_reports = get_xls_row_start
    xls_reports_per_day_bad_reports = get_xls_row_start

    @total_bad_reports = 0

    get_last_month.each do |day|
      @last_month_label += "\"" + day.strftime("%d/%m") + "\","

      xls_reports_per_day_label += get_xls_styled_cell("sd", "DateTime", day.strftime("%FT%T.%3N"))

      good_reports_from_day = 0
      bad_reports_from_day = 0

      @surveys.each do |key, survey|
        if Time.at(DateTime.parse(survey["createdAt"]).to_i).to_date === Time.at(day.to_i).to_date
          if survey["isGoodReport"] == true
            good_reports_from_day += 1
          else
            bad_reports_from_day += 1
          end
        end
      end

      @total_bad_reports += bad_reports_from_day

      @good_reports_from_last_month += good_reports_from_day.to_s + ","
      @bad_reports_from_last_month += bad_reports_from_day.to_s + ","

      xls_reports_per_day_good_reports += get_xls_cell("Number", good_reports_from_day.to_s)
      xls_reports_per_day_bad_reports += get_xls_cell("Number", bad_reports_from_day.to_s)
    end

    @last_month_label = @last_month_label.chomp(",")
    @good_reports_from_last_month = @good_reports_from_last_month.chomp(",")
    @bad_reports_from_last_month = @bad_reports_from_last_month.chomp(",")

    xls_reports_per_day_label += get_xls_row_end
    xls_reports_per_day_good_reports += get_xls_row_end
    xls_reports_per_day_bad_reports += get_xls_row_end

    @xls += get_xls_worksheet_header("ReportsPerDay")
    @xls += xls_reports_per_day_label
    @xls += xls_reports_per_day_good_reports
    @xls += xls_reports_per_day_bad_reports
    @xls += get_xls_worksheet_footer

    # Chart: Cities x Diseases

    @cities_label = "\"Bogotá\",\"Cartagena\",\"Medellín\",\"Villavicencio\",\"N/A\""

    @diseases_datasets = ""

    @total_diseases_city_chart = 0

    xls_cities_diseases_label = get_xls_row_start
    xls_cities_diseases_label += get_xls_cell("String", "Síndrome")
    xls_cities_diseases_label += get_xls_cell("String", "Bogotá")
    xls_cities_diseases_label += get_xls_cell("String", "Cartagena")
    xls_cities_diseases_label += get_xls_cell("String", "Medellín")
    xls_cities_diseases_label += get_xls_cell("String", "Villavicencio")
    xls_cities_diseases_label += get_xls_cell("String", "N/A")
    xls_cities_diseases_label += get_xls_row_end

    xls_cities_diseases_rows = ""

    @diseases.each do |key, disease|
      @diseases_datasets += "{label:'" + disease["name"] + "',"
      color = "%06x" % (rand * 0xffffff)
      @diseases_datasets += "backgroundColor:'#" + color.to_s + "',"
      @diseases_datasets += "borderColor:'#" + color.to_s + "',"
      @diseases_datasets += "fill: false,"
      @diseases_datasets += "data:["

      xls_cities_diseases_rows += get_xls_row_start
      xls_cities_diseases_rows += get_xls_cell("String", disease["name"])

      get_important_cities.each do |city|
        disease_score = 0

        @surveys.each do |key, survey|
          if survey["city"] == city and get_diagnostic(disease, survey) == "Si"
            disease_score += 1
          end
        end

        @diseases_datasets += disease_score.to_s + ","

        @total_diseases_city_chart += disease_score

        xls_cities_diseases_rows += get_xls_cell("Number", disease_score.to_s)
      end

      @diseases_datasets = @diseases_datasets.chomp(",")
      @diseases_datasets += "]},"

      xls_cities_diseases_rows += get_xls_row_end
    end

    @diseases_datasets = @diseases_datasets.chomp(",")

    @xls += get_xls_worksheet_header("DiseasesPerCity")
    @xls += xls_cities_diseases_label
    @xls += xls_cities_diseases_rows
    @xls += get_xls_worksheet_footer

    # Chart: Population Pyramid

    @age_groups_label = ""
    @quantity_of_males_per_age_group = ""
    @quantity_of_females_per_age_group = ""

    xls_population_pyramid_label = get_xls_row_start
    xls_age_groups = ""
    xls_population_pyramid_males = get_xls_row_start
    xls_population_pyramid_females = get_xls_row_start

    get_age_groups.each do |age_group|
      @age_groups_label += "\"" + age_group + "\","

      xls_age_groups += get_xls_cell("String", age_group)

      males_in_age_group = 0
      females_in_age_group = 0

      @users.each do |key, user|
        if user["ageGroup"] == age_group
          if user["gender"] == "Masculino"
            males_in_age_group -= 1
          else
            females_in_age_group +=1
          end
        end
      end

      @quantity_of_males_per_age_group += males_in_age_group.to_s + ","
      @quantity_of_females_per_age_group += females_in_age_group.to_s + ","

      xls_population_pyramid_males += get_xls_cell("Number", males_in_age_group.abs.to_s)
      xls_population_pyramid_females += get_xls_cell("Number", females_in_age_group.abs.to_s)
    end

    @age_groups_label = @age_groups_label.chomp(",")
    @quantity_of_males_per_age_group = @quantity_of_males_per_age_group.chomp(",")
    @quantity_of_females_per_age_group = @quantity_of_females_per_age_group.chomp(",")

    xls_population_pyramid_label += xls_age_groups + get_xls_row_end
    xls_population_pyramid_males += get_xls_row_end
    xls_population_pyramid_females += get_xls_row_end

    @xls += get_xls_worksheet_header("PopulationPyramid")
    @xls += xls_population_pyramid_label
    @xls += xls_population_pyramid_males
    @xls += xls_population_pyramid_females
    @xls += get_xls_worksheet_footer

    # Chart: Population Pyramid (Survey)

    @quantity_of_survey_males_per_age_group = ""
    @quantity_of_survey_females_per_age_group = ""

    xls_population_pyramid_survey_males = get_xls_row_start
    xls_population_pyramid_survey_females = get_xls_row_start

    get_age_groups.each do |age_group|
      males_in_age_group = 0
      females_in_age_group = 0

      @surveys.each do |key, survey|
        profile = survey["profile"]
        unless profile.nil?
          if profile["ageGroup"] == age_group
            if profile["gender"] == "Masculino"
              males_in_age_group -= 1
            else
              females_in_age_group +=1
            end
          end
        end
      end

      @quantity_of_survey_males_per_age_group += males_in_age_group.to_s + ","
      @quantity_of_survey_females_per_age_group += females_in_age_group.to_s + ","

      xls_population_pyramid_survey_males += get_xls_cell("Number", males_in_age_group.abs.to_s)
      xls_population_pyramid_survey_females += get_xls_cell("Number", females_in_age_group.abs.to_s)
    end

    @quantity_of_survey_males_per_age_group = @quantity_of_survey_males_per_age_group.chomp(",")
    @quantity_of_survey_females_per_age_group = @quantity_of_survey_females_per_age_group.chomp(",")

    xls_population_pyramid_survey_males += get_xls_row_end
    xls_population_pyramid_survey_females += get_xls_row_end

    @xls += get_xls_worksheet_header("SurveyPyramid")
    @xls += xls_population_pyramid_label
    @xls += xls_population_pyramid_survey_males
    @xls += xls_population_pyramid_survey_females
    @xls += get_xls_worksheet_footer

    # Chart: Age x Diseases

    @age_diseases_datasets = ""

    @total_diseases_age_chart = 0

    xls_diseases_per_age_label = get_xls_row_start
    xls_diseases_per_age_label += get_xls_cell("String", "Síndrome")
    xls_diseases_per_age_label += xls_age_groups + get_xls_row_end
    xls_diseases_per_age_rows = ""

    @diseases.each do |key, disease|
      @age_diseases_datasets += "{label:'" + disease["name"] + "',"
      color = "%06x" % (rand * 0xffffff)
      @age_diseases_datasets += "backgroundColor:'#" + color.to_s + "',"
      @age_diseases_datasets += "borderColor:'#" + color.to_s + "',"
      @age_diseases_datasets += "fill: false,"
      @age_diseases_datasets += "data:["

      xls_diseases_per_age_rows += get_xls_row_start
      xls_diseases_per_age_rows += get_xls_cell("String", disease["name"])

      get_age_groups.each do |age_group|
        disease_score = 0

        @surveys.each do |key, survey|
          user = survey["profile"]
          unless user.nil?
            if user["ageGroup"] == age_group and get_diagnostic(disease, survey) == "Si"
              disease_score += 1
            end
          end

        end

        @age_diseases_datasets += disease_score.to_s + ","

        @total_diseases_age_chart += disease_score

        xls_diseases_per_age_rows += get_xls_cell("Number", disease_score.to_s)
      end

      @age_diseases_datasets = @age_diseases_datasets.chomp(",")
      @age_diseases_datasets += "]},"

      xls_diseases_per_age_rows += get_xls_row_end
    end

    @age_diseases_datasets = @age_diseases_datasets.chomp(",")

    @xls += get_xls_worksheet_header("DiseasesPerAge")
    @xls += xls_diseases_per_age_label
    @xls += xls_diseases_per_age_rows
    @xls += get_xls_worksheet_footer
    @xls += get_xls_footer
    @xls.gsub!(/\s/,'%20')

    # Summary Infos

    @total_disease_reports = 0

    @cities_total = {
      cities: [ "Bogotá", "Cartagena", "Medellín", "Villavicencio"],
      reports: [ 0, 0, 0, 0 ],
      bad_reports: [ 0, 0, 0, 0],
      disease_reports: [ 0, 0, 0, 0]
    }

    @surveys.each do |skey, survey|
      current_city = @cities_total[:cities].index(survey["city"])

      if current_city
        @cities_total[:reports][current_city] += 1
        unless survey["isGoodReport"]
          @cities_total[:bad_reports][current_city] += 1
        end
      end

      @diseases.each do |dkey, disease|
        if get_diagnostic(disease, survey) == "Si"
          @total_disease_reports += 1
          if current_city
            @cities_total[:disease_reports][current_city] += 1
          end
          break
        end
      end
    end
  end

  private

  def get_xls_header
    header = "<?xml version=\"1.0\"?>"
    header += "<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\""
    header += " xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">"
    header
  end

  def get_xls_row_start
    "<Row>"
  end

  def get_xls_row_end
    "</Row>"
  end

  def get_xls_single_number_style(id, number_format)
    "<Styles><Style ss:ID=\"" + id + "\"><NumberFormat ss:Format=\"" + number_format + "\"/></Style></Styles>"
  end

  def get_xls_cell(type, data)
    "<Cell><Data ss:Type=\"" + type + "\">" + data + "</Data></Cell>"
  end

  def get_xls_styled_cell(id, type, data)
    "<Cell ss:StyleID=\"" + id + "\"><Data ss:Type=\"" + type + "\">" + data + "</Data></Cell>"
  end

  def get_xls_worksheet_header(name)
    "<Worksheet ss:Name=\"" + name + "\"><Table>"
  end

  def get_xls_worksheet_footer
    "</Table></Worksheet>"
  end

  def get_xls_footer
    "</Workbook>"
  end

  def get_last_month
    (DateTime.now.prev_month..DateTime.now)
  end

  def get_age_groups
    [ "80", "70_79", "60_69", "50_59", "40_49", "30_39", "20_29", "13_19", "00_12" ]
  end

  def get_important_cities
    [ "Bogotá", "Cartagena" , "Medellín" , "Villavicencio", "N/A" ]
  end
end
